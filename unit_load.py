# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from .tools import get_control_digit


class UnitLoad(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    @property
    def sscc(self):
        return self._get_sscc(self.id)

    def _get_sscc(self, code):
        Conf = Pool().get('product.configuration')

        conf = Conf(1)
        ean_identifier = [identifier for identifier in
            self.company.party.identifiers if identifier.type == 'ean']
        value = '00%(sscc_var)s%(ean_party)s%(code)s'
        ean_party = ean_identifier[0].code if ean_identifier else '000000'
        sequence_len = 16 - len(ean_party)
        value %= {
            'sscc_var': conf.sscc_variable,
            'ean_party': ean_party,
            'code': str(code).zfill(sequence_len)
        }
        return value + get_control_digit(value)
