# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import product
from . import party
from . import unit_load


def register():
    Pool.register(
        product.Product,
        product.Configuration,
        product.ConfigurationEAN,
        party.PartyIdentifier,
        module='product_ean', type_='model')
    Pool.register(
        product.CrossReference,
        product.ProductCrossReference,
        module='product_ean', type_='model',
        depends=['product_cross_reference'])
    Pool.register(
        unit_load.UnitLoad,
        module='product_ean', type_='model',
        depends=['stock_unit_load'])
