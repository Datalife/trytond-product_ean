# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
from decimal import Decimal
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
from trytond.pool import Pool
from trytond.modules.product_ean.tools import get_control_digit


class ProductEanTestCase(ModuleTestCase):
    """Test Product Ean module"""
    module = 'product_ean'

    @with_transaction()
    def test_product_ean(self):
        'Test product ean'
        pool = Pool()
        Uom = pool.get('product.uom')
        Conf = pool.get('product.configuration')
        Template = pool.get('product.template')
        Product = pool.get('product.product')
        Identifier = pool.get('product.identifier')

        conf = Conf(1)
        conf.gtin14_variable = '4'
        conf.save()
        kilogram, = Uom.search([
                ('name', '=', 'Kilogram'),
                ], limit=1)
        millimeter, = Uom.search([
                ('name', '=', 'Millimeter'),
                ])
        pt1, pt2 = Template.create([{
                    'name': 'P1',
                    'type': 'goods',
                    'list_price': Decimal(20),
                    'default_uom': kilogram.id,
                    'products': [('create', [{
                                    'code': '1',
                                    }])]
                    }, {
                    'name': 'P2',
                    'type': 'goods',
                    'list_price': Decimal(20),
                    'default_uom': millimeter.id,
                    'products': [('create', [{
                                    'code': '2',
                                    }])]
                    }])
        p1, = pt1.products
        p1.identifiers = [Identifier(type='ean', code='8437014044119')]
        p1.save()
        self.assertEqual(p1.gtin13(), '8437014044119')
        self.assertEqual(p1.gtin14(), '48437014044117')

        self.assertEqual(
            Product.search([('ean_code', '=', '8437014044119')]), [p1])

    @with_transaction()
    def test_digit_control(self):
        """Test digit control"""
        self.assertEqual(get_control_digit('4843701404411'), '7')
        self.assertEqual(get_control_digit('48437014044000557'), '0')
        self.assertEqual(get_control_digit('48437014044000130'), '5')


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            ProductEanTestCase))
    return suite
